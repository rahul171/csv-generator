const fs = require('fs');

const dir = 'output';

fs.readdir('output', (err, files) => {
  files.filter(file => file !== '.keep').forEach(file => {
    const { size } = fs.statSync(`${dir}/${file}`);
    const kb = (size / 1024).toFixed(2);
    const mb = (size / 1024 / 1024).toFixed(2);
    console.log(`${file} : ${kb}kb - ${mb}mb`);
  })
})
