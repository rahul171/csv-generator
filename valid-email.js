const fs = require('fs');
const { rs } = require('./helpers');

const argsv = process.argv.slice(2);

if (!argsv || argsv.length !== 5) {
  console.log('4 params required');
  process.exit(1);
}

const args = argsv.slice(0, 4).map(arg => Number(arg));

const rows = args[0];
const cols = args[1];
const cellLength = args[2];
const suffix = args[3];
const email = argsv[4];

const filename = `output/sample-valid-emails-${rows}-${cols}-${cellLength}-${suffix}.csv`;

const ws = fs.createWriteStream(filename, { flags: 'w' });

for (let i = 0; i < rows + 1; i++) {
  for (let j = 0; j < cols; j++) {
    let str = rs(cellLength);
    if (j === 0) {
      if (i === 0) {
        str = 'Email';
      } else {
        const parts = email.split('@');
        str = `${parts[0]}+${suffix}.${i}@${parts[1]}`;
      }
    }
    if (j !== cols - 1) {
      str += ',';
    }
    ws.write(str);
  }
  ws.write('\n');
}

ws.close();

console.log(`${filename} created`);
