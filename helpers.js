const rn = (a = 0, b = 1) => a + Math.floor(Math.random() * (b - a + 1));

const rs = (length = 10) => {
  const ranges = [[65, 70], [97, 122], [48, 57]];
  return Array(length).fill(null).reduce((a) => {
    const range = ranges[rn(0, ranges.length - 1)];
    return a + String.fromCharCode(rn(range[0], range[1]))
  }, '');
}

module.exports = { rn, rs }
