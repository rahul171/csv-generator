## CSV Generator

### Usage

##### Generate a CSV:
```shell script
node index.js [rows] [cols] [cellLength]
```

##### Generate a CSV with Email Column:
```shell script
node email.js [rows] [cols] [cellLength] [suffix]
```

##### Generated Files' Stats
```shell script
node stats.js
```
