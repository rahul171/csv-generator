const fs = require('fs');
const { rs } = require('./helpers');

const args = process.argv.slice(2).map(arg => Number(arg));

if (!args || args.length !== 3) {
  console.log('3 params required');
  process.exit(1);
}

const rows = args[0];
const cols = args[1];
const cellLength = args[2];

const filename = `output/sample-${rows}-${cols}-${cellLength}.csv`;

const ws = fs.createWriteStream(filename, { flags: 'w' });

for (let i = 0; i < rows + 1; i++) {
  for (let j = 0; j < cols; j++) {
    let str = rs(cellLength);
    if (j !== cols - 1) {
      str += ',';
    }
    ws.write(str);
  }
  ws.write('\n');
}

ws.close();

console.log(`${filename} created`);
